package lendable;

import java.util.Date;

/**
 * @author Chrisantus A. Khamasi
 * Class modelling a customer account
 */
public class CustomerAccount {
    private String accountNo;
    private double transactionAmount;
    private Date transactionDate;

    public CustomerAccount(String accountNo, double transactionAmount, Date transactionDate) {
        this.accountNo = accountNo;
        this.transactionAmount = transactionAmount;
        this.transactionDate = transactionDate;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

}
