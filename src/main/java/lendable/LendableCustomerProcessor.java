package lendable;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author Chrisantus A. Khamasi
 * Class to process customer information
 */
public class LendableCustomerProcessor {

    private Logger logger = Logger.getLogger("LendableCustomerProcessor");

    /**
     * Get the top n best customers with good payment history.
     * A good payment history is defined by longest consecutive daily payment period
     *
     * @param filePath          The input file path
     * @param numberOfCustomers The number of results expected
     * @return Best n Daily Paying Grouped Customer Accounts
     */

    /**
     * Get the top n best customers with good payment history.
     * A good payment history is defined by longest consecutive daily payment period
     *
     * @param filePath          The input file path
     * @param numberOfCustomers The number of results expected
     * @return @{@link Map} having the best <b>n</b> daily paying grouped customer accounts with the account no as
     * key and
     * longest daily payment duration as value
     */
    public Map<String, Integer> getBestDailyPayingCustomer(String filePath, int numberOfCustomers) {
        List<CustomerAccount> fullCustomerList = new ArrayList<>();
        File inputFile = new File(filePath);
        if (inputFile == null) throw new RuntimeException("Invalid file path " + filePath);

        //Read file and map the customers
        try {
            logger.log(Level.INFO, "opening file " + filePath);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile));
            //read header
            String row = bufferedReader.readLine();
            //read first line
            row = bufferedReader.readLine();
            int pos = 1;
            logger.log(Level.INFO, "reading customer account data");
            while (row != null) {
                String[] customerData = row.split(",");
                try {
                    fullCustomerList.add(new CustomerAccount(customerData[0], Double.valueOf(customerData[1]), new
                            SimpleDateFormat("yyyy-MM-dd").parse(customerData[2].split(" ")[0])));
                } catch (ParseException e) {
                    logger.log(Level.SEVERE, "could not open parse date for customer account  " + customerData[0], e);
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "could not process data for customer line number " + pos,
                            e);
                }
                pos++;
                row = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE, "could not open file", e);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "could not read file", e);
        }

        //Group customers by their account numbers
        Map<String, List<CustomerAccount>> groupedCustomerMap = fullCustomerList.stream().collect(Collectors
                .groupingBy(CustomerAccount::getAccountNo));
        Map<String, Integer> customerRanking = new HashMap<>();

        //Find longest consecutive daily payment history
        groupedCustomerMap.forEach((key, list) -> {
            list.sort((account1, account2) -> {
                return account1.getTransactionDate().before(account2.getTransactionDate()) ? -1 : 0;
            });
            CustomerAccount previousAccount = null;
            int longestDuration = 0;
            int currentPaymentDuration = 0;
            for (CustomerAccount customerAccount : list) {
                if (previousAccount == null) {
                    //this is the first account so no calculations can be done
                    previousAccount = customerAccount;
                    continue;
                } else {
                    long timeDifference = customerAccount.getTransactionDate().getTime() - previousAccount
                            .getTransactionDate().getTime
                                    ();
                    String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(customerAccount.getTransactionDate
                            ());
                    String previousDate = new SimpleDateFormat("yyyy-MM-dd").format(previousAccount
                            .getTransactionDate());

                    if (currentDate.equals
                            (previousDate)) {
                        //payment made the same day; skip
                        continue;
                    }
                    if (timeDifference > 0 && timeDifference <= 24 * 60 * 60 * 1000) {
                        //add one day to the consecutive payment duration
                        currentPaymentDuration++;
                        previousAccount = customerAccount;
                        //update the consecutive longest duration counter
                        if (currentPaymentDuration > longestDuration) longestDuration = currentPaymentDuration;
                    } else {
                        //we are past one day, reset the consecutive day counter
                        if (currentPaymentDuration > longestDuration) longestDuration = currentPaymentDuration;
                        currentPaymentDuration = 0;
                        previousAccount = customerAccount;
                    }
                }
            }
            customerRanking.put(previousAccount.getAccountNo(), longestDuration);

        });

        //Hold the customer accounts in a list for sorting
        List<String> customerRankingList = new LinkedList<>();
        customerRanking.forEach((key, value) -> {
            customerRankingList.add(key);
        });

        //sort customers by longest duration and if there is a tie, break it by customer id
        customerRankingList.sort((a, b) -> {
            boolean tie = customerRanking.get(a).intValue() == customerRanking.get(b).intValue();
            if (tie) {
                return a.compareTo(b);
            } else {
                return customerRanking.get(a).intValue() > customerRanking.get(b).intValue() ? -1 : 1;
            }

        });

        //add the n number of customers to return object
        Map<String, Integer> rankedCustomers = new LinkedHashMap<>();
        for (int i = 0; i < numberOfCustomers && i < customerRankingList.size(); i++) {
            rankedCustomers.put(customerRankingList.get(i), customerRanking.get(customerRankingList.get(i)));
        }
        return rankedCustomers;
    }
}
