package lendable;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Please give the filepath and the number of customers as parameters to rate e.g " +
                    "lendable.jar customers.csv 2");
            System.exit(-1);
        } else if (args.length > 2) {
            System.out.println("Too many parameters. Please give the filepath and the number of customers as " +
                    "parameters to rate e.g \" +\n" +
                    "                    \"lendable_customer_rank.jar customers.csv 2");
            System.exit(-1);
        }
        LendableCustomerProcessor lendableCustomerProcessor = new LendableCustomerProcessor();
        Map<String, Integer> rankedCustomers = lendableCustomerProcessor.getBestDailyPayingCustomer(args[0], Integer
                .valueOf(args[1]));
        System.out.println("\n\nBest "+args[1]+" Customers : \n");
        rankedCustomers.forEach((key, value) -> System.out.println(key + " -> " + value));

    }
}
