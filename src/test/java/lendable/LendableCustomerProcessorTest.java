package lendable;

import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class LendableCustomerProcessorTest {


    @org.junit.Test
    public void getBestDailyPayingCustomer() {
        LendableCustomerProcessor lendableCustomerProcessor = new LendableCustomerProcessor();
        String filePath = "transaction_data_1.csv";
        String filePath2 = "transaction_data_2.csv";
        String filePath3 = "transaction_data_3.csv";
        int numberOfCustomers = 3;

        //Test 1
        Map<String,Integer> rankedCustomerMap = lendableCustomerProcessor.getBestDailyPayingCustomer(filePath,
                numberOfCustomers);
        List<String> rankedCustomerList = new LinkedList<>();
        rankedCustomerMap.forEach((key,value)->rankedCustomerList.add(key));
        assertEquals("Test criteria 1 was not met. ","ACC2",rankedCustomerList.get(0));

        //Test 2
        rankedCustomerMap = lendableCustomerProcessor.getBestDailyPayingCustomer(filePath2,
                numberOfCustomers);
        List<String> rankedCustomerList2 = new LinkedList<>();
        rankedCustomerMap.forEach((key,value)->rankedCustomerList2.add(key));
        assertEquals("Test criteria 2 was not met. ","ACC1",rankedCustomerList2.get(0));

        //Test 3
        rankedCustomerMap = lendableCustomerProcessor.getBestDailyPayingCustomer(filePath3,
                numberOfCustomers);
        List<String> rankedCustomerList3 = new LinkedList<>();
        rankedCustomerMap.forEach((key,value)->rankedCustomerList3.add(key));
        assertEquals("Test criteria 3 was not met. ","ACC143",rankedCustomerList3.get(0));
    }
}
